<?php
namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Expense;
use App\Entity\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create('fr_FR');

        //User
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
           
            $user->setUsername($faker->firstName(null));
            $user->setPassword($faker->sentence($nbWords = 1));
            $user->setEmail($faker->email());
            $user->setRoles(['ROLE_USER']);

            $manager->persist($user);
        }

        $manager->flush();

        //Category
        for ($i = 0; $i < 20; $i++) {
            $cat = new Category();
           
            $cat->setName($faker->sentence($nbWords = 1));
            $cat->setDescription($faker->sentence($nbWords = 20));
            
            $manager->persist($cat);
        }

        $manager->flush();

        //Expense
        for ($i = 0; $i < 20; $i++) {
            $e = new Expense();
           
            $e->setName($faker->sentence($nbWords = 1));
            $e->setSum($faker->randomFloat(2) );
            $e->setDate($faker->dateTimeBetween('-15 week', '+0 week'));
            //$e->setCategory($faker->randomDigitNotNull());
            //$e->setUser($faker->randomDigitNotNull());

            $manager->persist($e);
        }

        $manager->flush();
    }
}