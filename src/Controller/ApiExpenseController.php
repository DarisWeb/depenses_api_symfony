<?php

namespace App\Controller;

use App\Entity\Expense;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\ExpenseRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Annotations\AnnotationReader;
use Error;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;

class ApiExpenseController extends AbstractController
{
    /**
     * @Route("/", name="test")
     */
    public function index(): Response
    {
        return $this->render('expense/index.html.twig', [
            'controller_name' => 'ExpenseController',
        ]);
    }



    /**
     * @Route("/api/expenses", name="expenses", methods={"GET"})
     * @param ExpenseRepository $repo
     * @return Response
     */
    public function expenses(ExpenseRepository $repo): Response
    {
        $user = $this->getUser();
        $result = $repo->findBy(['user' => $user]);
        return $this->json($result, 200, [], ['groups' => 'group1']);
    }

    /**
     * @Route("/api/newexpense", name="newexpense", methods={"POST"})
     * @return Response
     */
    public function addExpense(Request $request, UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        //dd($request);
        $data = $request->getContent();
        $relation = $request->toArray();
        //dump($relation);
        try {
            $object = $this->get('serializer')->deserialize($data, Expense::class, 'json');
        }catch (Exception $e){
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
        dump($object);
        $object->setDate(new \DateTime());
        $user = $userRepository->findOneBy(array('id' => $relation['user'] ));
        $categ = $categoryRepository->findOneBy(array('id' => $relation['category']));

        $object->setUser($user);
        $object->setCategory($categ);


        dump($data);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($object);
        $entityManager->flush();

        return new Response(
            '<html><body>ajout de données</body></html>'
        );

    }

    /**
     * @Route("/api/delexpense/{id}", name="delexpense", methods={"DELETE"})
     * @return Response
     */
    public function deleteExpense(Expense $expense): Response
    {
        
        if ($expense) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($expense);
            $em->flush();
        }
        return new Response(null, 204);
    }




    /**
     * @Route("/api/user/register", name="userregoifdg", methods={"POST"})
     * @return Response
     */
    public function fdsfdf(Request $request): Response
    {
        $user = new User();
        $user->setUsername($request->get('username'));


        try {
            $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

        } catch(Error $e) {
            return new JsonResponse('inscription invalide'. $e->getMessage());
        }
     
        return new JsonResponse('inscription ok', 201);
    }

}